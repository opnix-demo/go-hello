# Go Web sample program

## Project Introduction

This project is a simple one [Golang](http://golang.org) Web application example, directory structure:

```
.
├── Godeps
│   ├── Godeps.json
│   ├── Readme
│   └── _workspace
├── hello
│   └── main.go
├── Procfile
├── README.md
└── static
    └── index.html
```

## Project requirements

Need to have the root directory of the project `Godeps/Godeps.json` to specify `Golang` version and related dependencies，Recommended Use [Godep](https://github.com/tools/godep) managing project dependencies can speed up project deployment. Also need a `Procfile` File to specify the application's startup command, for  go for this reason, this command is the name of the executable file.
